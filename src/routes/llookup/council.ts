import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';
import { CouncilModel } from '../../models/llookup/council';

// schema
import createSchema from '../../schema/admit/create';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const importService = new CouncilModel();

  //Info
  fastify.get('/', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      // fastify.guard.scope('order.create','admit.read')
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const data:any = await importService.info(db);
      const rsTotal: any = await importService.infoTotal(db);
      return reply.status(StatusCodes.CREATED).send({ ok: true ,data,total: Number(rsTotal[0].total)});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  //Info By Id
  fastify.get('/:id', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      // fastify.guard.scope('order.create','admit.read')
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const id:any = req.params.id;
    try {
      const data:any = await importService.infoByID(db,id);

      return reply.status(StatusCodes.CREATED).send({ ok: true ,data});

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })
}