import { FastifyInstance } from "fastify";

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  // verify jwt token
  fastify.addHook("onRequest", (request) => request.jwtVerify());

  fastify.register(require('./activity'), { prefix: '/lookup/lactivity' });
  fastify.register(require('./council'), { prefix: '/lookup/council' });
  fastify.register(require('./discharge_status'), { prefix: '/lookup/discharge-status' });
  fastify.register(require('./discharge_type'), { prefix: '/lookup/discharge-type' });
  fastify.register(require('./evaluate'), { prefix: '/lookup/levaluate' });
  fastify.register(require('./group_disease'), { prefix: '/lookup/group-disease' });
  fastify.register(require('./hospital'), { prefix: '/lookup/hospital' });
  fastify.register(require('./icd10'), { prefix: '/lookup/icd10' });
  fastify.register(require('./medicine_type'), { prefix: '/lookup/medicine-type' });
  fastify.register(require('./move_type'), { prefix: '/lookup/move-type' });
  fastify.register(require('./order_type'), { prefix: '/lookup/order-type' });
  fastify.register(require('./physical_examination'), { prefix: '/lookup/physical-examination' });
  fastify.register(require('./progress_note_type'), { prefix: '/lookup/progress-note-type' });
  fastify.register(require('./refer_type'), { prefix: '/lookup/refer-type' });
  fastify.register(require('./review_of_system'), { prefix: '/lookup/review-of-system' });

  done();

} 
