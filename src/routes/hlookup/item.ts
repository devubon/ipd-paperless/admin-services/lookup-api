import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';
import { ItemModel } from '../../models/hlookup/item';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const importService = new ItemModel();
  //List
  fastify.get('/list', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      // fastify.guard.scope('order.create','admit.read')
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    const params: any = request.params;

    try {
      const data: any = await importService.getByItem(db);

      return reply.status(StatusCodes.OK).send({ ok: true, data });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })
    //itemTypeID
    fastify.get('/item-type/:itemTypeID', {
      preHandler: [
        fastify.guard.role('admin','doctor','nurse'),
        // fastify.guard.scope('order.create','admit.read')
      ],
    }, async (request: FastifyRequest, reply: FastifyReply) => {
  
      const params: any = request.params;
      const itemTypeID: number = params.itemTypeID;
  
      try {
        const data: any = await importService.getByItemTypeID(db, itemTypeID);
  
        return reply.status(StatusCodes.OK).send({ ok: true, data });
  
      } catch (error: any) {
        request.log.info(error.message);
        return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
      }
    })
      //Save
  fastify.post('/', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      // fastify.guard.scope('order.create','admit.read')
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const datas:object = req.body;
    try {
      const data:any = await importService.save(db,datas);
      return reply.status(StatusCodes.OK).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })  

  //Update
  fastify.put('/:id', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      // fastify.guard.scope('order.create','admit.read')
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const datas:object = req.body;
    const id:any = req.params.id;

    try {
      const data:any = await importService.update(db,datas,id);
      return reply.status(StatusCodes.OK).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })  

  //Delete
  fastify.delete('/:id', {
    preHandler: [
      fastify.guard.role('admin','doctor','nurse'),
      // fastify.guard.scope('order.create','admit.read')
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const id:any = req.params.id;

    try {
      const data:any = await importService.delete(db,id);
      return reply.status(StatusCodes.OK).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  }) 
    //getByID
    fastify.get('/getByID/:id', {
      preHandler: [
        fastify.guard.role('admin','doctor','nurse'),
        // fastify.guard.scope('order.create','admit.read')
      ],
    }, async (request: FastifyRequest, reply: FastifyReply) => {
  
      const params: any = request.params;
      const id: number = params.id;
  
      try {
        const data: any = await importService.getByID(db, id);
  
        return reply.status(StatusCodes.OK).send({ ok: true, data });
  
      } catch (error: any) {
        request.log.info(error.message);
        return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
      }
    })
  done();
}