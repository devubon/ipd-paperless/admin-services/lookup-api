import { FastifyInstance } from "fastify";

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  // verify jwt token
  fastify.addHook("onRequest", (request) => request.jwtVerify());

  fastify.register(require('./doctor'), { prefix: '/lookup/doctor' });

  fastify.register(require('./department'), { prefix: '/lookup/department' });
  fastify.register(require('./insurance'), { prefix: '/lookup/insurance' });
  fastify.register(require('./ward'), { prefix: '/lookup/ward' });
  fastify.register(require('./food'), { prefix: '/lookup/food' });
  fastify.register(require('./medicine'), { prefix: '/lookup/medicine' });
  fastify.register(require('./item'), { prefix: '/lookup/items' });
  fastify.register(require('./item_type'), { prefix: '/lookup/item_type' });
  fastify.register(require('./bed_type'), { prefix: '/lookup/bed_type' });
  fastify.register(require('./diagnosis'), { prefix: '/lookup/diagnosis' });
  fastify.register(require('./medicine_usage'), { prefix: '/lookup/medicine_usage' });
  fastify.register(require('./refer_by'), { prefix: '/lookup/refer_by' });
  fastify.register(require('./refer_cause'), { prefix: '/lookup/refer_cause' });
  fastify.register(require('./specialist'), { prefix: '/lookup/specialist' });

  done();

} 
