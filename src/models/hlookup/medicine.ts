import { UUID } from 'crypto';
import { Knex } from 'knex';
export class MedicineModel {

  list(db: Knex, search: any, limit: number, offset: number) {
    let sql = db('h_medicine')

    if (search) {
      let _search = `%${search}%`
      sql.where(builder => {
        builder.whereRaw('LOWER(name) like LOWER(?)', [_search])
      })
    }

    sql.where('is_active', true)

    return sql.limit(limit).offset(offset).orderBy('name')
  }

  listTotal(db: Knex, search: any) {
    let sql = db('h_medicine')
    if (search) {
      let _search = `%${search}%`
      sql.where(builder => {
        builder.whereRaw('LOWER(name) like LOWER(?)', [_search])
      })
    }
    return sql.where('is_active', true).count({ total: '*' });
  }

  infoByID(db: Knex, id: UUID) {
    let sql = db('h_medicine')
    return sql.where('is_active', true).andWhere('id', id)
      .orderBy('name')
  }
  save(db: Knex,data:object){
    let sql = db('h_medicine')
    return sql.insert(data);
  }

  update(db: Knex,data:object,id:UUID){
    let sql = db('h_medicine')
    return sql.update(data).where('id',id);
  }

  delete(db: Knex,id:UUID){
    let sql = db('h_medicine')
    return sql.delete().where('id',id);
  } 
}