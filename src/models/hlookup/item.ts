import { UUID } from 'crypto';
import { Knex } from 'knex';

export class ItemModel {
  getByItemTypeID(db: Knex, itemTypeID: number) {
    return db('h_items')
      .where('item_type_id', itemTypeID)
      .andWhere('is_active', true)
      .select('id', 'item_type_id', 'code', 'name', 'reference_code', 'price', 'unit', 'provider_allow')
  }
  getByItem(db: Knex) {
    return db('h_items')
      .andWhere('is_active', true)
      .select('id', 'item_type_id', 'code', 'name', 'reference_code', 'price', 'unit', 'provider_allow')
  }
  save(db: Knex,data:object){
    let sql = db('h_items')
    return sql.insert(data);
  }

  update(db: Knex,data:object,id:UUID){
    let sql = db('h_items')
    return sql.update(data).where('id',id);
  }

  delete(db: Knex,id:UUID){
    let sql = db('h_items')
    return sql.delete().where('id',id);
  } 
  getByID(db: Knex, id: number) {
    return db('h_items')
      .where('id', id)
      .andWhere('is_active', true)
      .select('id', 'item_type_id', 'code', 'name', 'reference_code', 'price', 'unit', 'provider_allow')
  } 
}