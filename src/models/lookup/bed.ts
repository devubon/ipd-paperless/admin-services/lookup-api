import { UUID } from 'crypto';
import { Knex } from 'knex';
export class BedModel {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  
  list(db: Knex) {
    let sql = db('bed')
    return sql.select()
    .orderBy('name')
  }

  info(db: Knex) {
    let sql = db('bed')
    return sql.select()
    .where('is_active',true)
    .orderBy('name')
  }

  infoTotal(db: Knex) {
    let sql = db('bed')
    return sql.where('is_active',true)
    .count({ total: '*' });
  }

  infoByID(db: Knex,id:UUID) {
    let sql = db('bed')
    return sql.select('bed.id as bed_id','bed.name','bed.ward_id','h_bed_type.name as type_name','standard_price','extra_paid')
    .innerJoin('h_bed_type','h_bed_type.id','bed.bed_type_id')
    .where('bed.is_active',true)
    .where('bed.id',id)
    .orderBy('bed.name')
  }

  infoByWardID(db: Knex,ward_id:UUID) {
    let sql = db('bed')
    return sql.select('bed.id as bed_id','bed.name','bed.ward_id','h_bed_type.name as type_name','standard_price','extra_paid')
    .innerJoin('h_bed_type','h_bed_type.id','bed.bed_type_id')
    .where('bed.is_active',true)
    .where('bed.ward_id',ward_id)
    .orderBy('bed.name')
  }

  save(db: Knex,data:object){
    let sql = db('bed')
    return sql.insert(data);
  }

  update(db: Knex,data:object,id:UUID){
    let sql = db('bed')
    return sql.update(data).where('id',id);
  }

  delete(db: Knex,id:UUID){
    let sql = db('bed')
    return sql.delete().where('id',id);
  }

}